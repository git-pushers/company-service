-- +goose Up
-- +goose StatementBegin
CREATE TABLE company
(
    id          bigserial PRIMARY KEY,
    user_id     bigint    NOT NULL,
    name        text      NOT NULL,
    description text      NOT NULL,
    created_at  timestamp NOT NULL,
    updated_at  timestamp NOT NULL,
    deleted_at  timestamp
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE company;
-- +goose StatementEnd
