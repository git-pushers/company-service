-- +goose Up
-- +goose StatementBegin
CREATE TABLE photo
(
    uuid       uuid,
    url        text,
    is_main    bool,
    company_id bigint
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP table photo;
-- +goose StatementEnd
