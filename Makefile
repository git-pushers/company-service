BIN = $(PWD)/bin
BUILD = $(PWD)/bin/build/$(PACKAGE)
MAIN = $(PWD)/cmd/$(PACKAGE)

GOLANGCI_LINT_VER = v1.55.2

.PHONY: lint
lint: bin/golangci-lint
	$(BIN)/golangci-lint run ./...

bin/golangci-lint:
	GOBIN=$(BIN) go install github.com/golangci/golangci-lint/cmd/golangci-lint@$(GOLANGCI_LINT_VER)

generate:
	protoc --go_out pkg --go_opt paths=source_relative \
		-I api \
	  --go-grpc_out pkg --go-grpc_opt=paths=source_relative \
	  --grpc-gateway_out pkg --grpc-gateway_opt paths=source_relative  \
	  --openapiv2_out api \
	  api/company/company.proto



