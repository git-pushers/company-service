package main

import (
	"context"
	"log"

	"gitlab.com/git-pushers/company-service/internal/app/company"
	"gitlab.com/git-pushers/company-service/internal/db"
	companyRepository "gitlab.com/git-pushers/company-service/internal/repository/company-repository"
)

func main() {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	pool, err := db.New(ctx)
	if err != nil {
		log.Fatalf("failed to init db")
	}
	repository := companyRepository.New(pool)

	go runHTTP(ctx)
	runGRPC(ctx, company.New(repository))
}
