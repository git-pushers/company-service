package main

import (
	"context"
	"gitlab.com/git-pushers/company-service/internal/docs"
	"log"
	"net/http"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	httpSwagger "github.com/swaggo/http-swagger/v2"
	descCompany "gitlab.com/git-pushers/company-service/pkg/company"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func runHTTP(ctx context.Context) {
	mux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithTransportCredentials(insecure.NewCredentials())}
	err := descCompany.RegisterCompanyServiceHandlerFromEndpoint(ctx, mux, "localhost:12202", opts)
	if err != nil {
		panic(err)
	}

	docs.SwaggerInfo.Host = "localhost:8080"
	docs.SwaggerInfo.BasePath = "/"

	if err := mux.HandlePath("GET", "/swagger/*", func(w http.ResponseWriter, r *http.Request, pathParams map[string]string) {
		httpSwagger.WrapHandler(w, r)
	}); err != nil {
		log.Fatalf("failed to init swagger")
	}

	if err := http.ListenAndServe(":8080", mux); err != nil {
		panic(err)
	}
}
