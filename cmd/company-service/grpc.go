package main

import (
	"context"
	"log"
	"net"

	descCompany "gitlab.com/git-pushers/company-service/pkg/company"
	"google.golang.org/grpc"
)

func runGRPC(ctx context.Context, server descCompany.CompanyServiceServer) {
	lis, err := net.Listen("tcp", ":12202")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	descCompany.RegisterCompanyServiceServer(s, server)

	if err := s.Serve(lis); err != nil {
		panic(err)
	}
}
