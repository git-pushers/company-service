package company

import "time"

// Company компания
type Company struct {
	ID          int64
	Name        string
	Description string
	Photos      []Photo
	UserID      int64
	CreatedAt   time.Time
	UpdatedAt   time.Time
	DeletedAt   *time.Time
}
