package company

import (
	"github.com/google/uuid"
)

// Photo фотография
type Photo struct {
	UUID   uuid.UUID
	URL    string
	IsMain bool
}
