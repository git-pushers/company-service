package db

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"
)

// New ...
func New(ctx context.Context) (*pgxpool.Pool, error) {
	pool, err := pgxpool.Connect(ctx, generateDSN())
	if err != nil {
		return nil, err
	}
	return pool, nil
}

func generateDSN() string {
	return fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", "localhost", 5437, "postgres", "postgres", "postgres")
}
