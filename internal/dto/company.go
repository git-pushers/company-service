package dto

import "gitlab.com/git-pushers/company-service/internal/domain/company"

type CreateCompanyRequest struct {
	Name        string
	Description string
	UserID      int64
	Photos      []company.Photo
}
