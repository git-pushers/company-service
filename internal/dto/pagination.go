package dto

// Pagination пагинация
type Pagination struct {
	LastID int64
	Limit  int64
}
