package company

import (
	"context"

	desc "gitlab.com/git-pushers/company-service/pkg/company"
)

func (i *impl) ListCompanies(context.Context, *desc.ListCompaniesRequest) (*desc.ListCompaniesResponse, error) {
	return nil, nil
}
