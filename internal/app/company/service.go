package company

import (
	"context"

	"gitlab.com/git-pushers/company-service/internal/dto"
	desc "gitlab.com/git-pushers/company-service/pkg/company"
)

type companyService interface {
	CreateCompany(ctx context.Context, req dto.CreateCompanyRequest) (int64, error)
}

type impl struct {
	*desc.UnimplementedCompanyServiceServer

	companyService companyService
}

// New реализация company-service
func New(service companyService) *impl {
	return &impl{
		companyService: service,
	}
}
