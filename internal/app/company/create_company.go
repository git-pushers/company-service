package company

import (
	"context"

	"github.com/google/uuid"
	"github.com/life4/genesis/slices"
	"gitlab.com/git-pushers/company-service/internal/domain/company"
	"gitlab.com/git-pushers/company-service/internal/dto"
	desc "gitlab.com/git-pushers/company-service/pkg/company"
)

func (i *impl) CreateCompany(ctx context.Context, req *desc.CreateCompanyRequest) (*desc.CreateCompanyResponse, error) {
	id, err := i.companyService.CreateCompany(ctx, dto.CreateCompanyRequest{
		Name:        req.Name,
		Description: req.Description,
		UserID:      req.UserId,
		Photos:      toPhotos(req.Photos),
	})
	if err != nil {
		return nil, err
	}
	return &desc.CreateCompanyResponse{
		Id: id,
	}, nil
}

func toPhotos(photos []*desc.Photo) []company.Photo {
	return slices.Map(photos, toPhoto)
}

func toPhoto(p *desc.Photo) company.Photo {
	return company.Photo{
		UUID:   uuid.New(),
		URL:    p.Url,
		IsMain: p.IsMain,
	}
}
