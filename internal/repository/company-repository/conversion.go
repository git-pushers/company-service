package company_repository

import (
	"time"

	"github.com/life4/genesis/slices"
	"gitlab.com/git-pushers/company-service/internal/domain/company"
	"gitlab.com/git-pushers/company-service/internal/dto"
)

func toCompanyEntity(c company.Company) companyEntity {
	return companyEntity{
		ID:          c.ID,
		Name:        c.Name,
		Description: c.Description,
		UserID:      c.UserID,
		CreatedAt:   c.CreatedAt,
		UpdatedAt:   c.UpdatedAt,
		DeletedAt:   c.DeletedAt,
	}
}

func toCreateCompanyEntity(c dto.CreateCompanyRequest, ct time.Time) companyEntity {
	return companyEntity{
		Name:        c.Name,
		Description: c.Description,
		UserID:      c.UserID,
		CreatedAt:   ct,
		UpdatedAt:   ct,
	}
}

func toPhotoEntities(photos []company.Photo, companyID int64) []photoEntity {
	return slices.Map(photos, func(p company.Photo) photoEntity { return toPhotoEntity(p, companyID) })
}

func toPhotoEntity(p company.Photo, companyID int64) photoEntity {
	return photoEntity{
		UUID:      p.UUID,
		URL:       p.URL,
		IsMain:    p.IsMain,
		CompanyID: companyID,
	}
}
