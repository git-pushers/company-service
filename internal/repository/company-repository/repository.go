package company_repository

import (
	"context"
	"time"

	"github.com/Masterminds/squirrel"
	"github.com/georgysavva/scany/pgxscan"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/git-pushers/company-service/internal/dto"
)

type repository struct {
	cluster *pgxpool.Pool
}

func New(cluster *pgxpool.Pool) *repository {
	return &repository{
		cluster: cluster,
	}
}

func (r *repository) CreateCompany(ctx context.Context, req dto.CreateCompanyRequest) (int64, error) {
	tx, err := r.cluster.BeginTx(ctx, pgx.TxOptions{})
	defer func() {
		_ = tx.Rollback(ctx)
	}()
	if err != nil {
		return 0, err
	}
	ct := time.Now()

	qb := squirrel.Insert("company").Columns("name", "description", "user_id", "created_at", "updated_at").
		Values(req.Name, req.Description, req.UserID, ct, ct).PlaceholderFormat(squirrel.Dollar).Suffix("RETURNING id")
	sql, args, err := qb.ToSql()
	if err != nil {
		return 0, err
	}

	var id int64

	err = pgxscan.Get(ctx, r.cluster, &id, sql, args...)
	if err != nil {
		return 0, err
	}

	qb = squirrel.Insert("photo").Columns("uuid", "url", "is_main", "company_id").PlaceholderFormat(squirrel.Dollar)
	for _, p := range req.Photos {
		qb = qb.Values(p.UUID, p.URL, p.IsMain, id)
	}
	sql, args, err = qb.ToSql()
	if err != nil {
		return 0, err
	}

	_, err = r.cluster.Exec(ctx, sql, args...)
	if err != nil {
		return 0, err
	}

	err = tx.Commit(ctx)
	if err != nil {
		return 0, err
	}
	return id, nil
}
