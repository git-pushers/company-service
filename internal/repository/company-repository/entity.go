package company_repository

import (
	"time"

	"github.com/google/uuid"
)

// companyEntity компания
type companyEntity struct {
	ID          int64      `db:"id"`
	Name        string     `db:"name"`
	Description string     `db:"description"`
	UserID      int64      `db:"user_id"`
	CreatedAt   time.Time  `db:"created_at"`
	UpdatedAt   time.Time  `db:"updated_at"`
	DeletedAt   *time.Time `db:"deleted_at"`
}

// photoEntity фотография
type photoEntity struct {
	UUID      uuid.UUID `db:"uuid"`
	URL       string    `db:"url"`
	IsMain    bool      `db:"is_main"`
	CompanyID int64     `db:"company_id"`
}
